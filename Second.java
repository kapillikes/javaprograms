interface FirstInterface{
default void print()
{
System.out.println("default method");
}
static void staticMethod()
{
System.out.println("static method");
}
void hello();
}
interface SecondInterface extends FirstInterface{
void hello();
}
class First {

}
class Second  extends First implements SecondInterface,FirstInterface  {
public void hello(){
System.out.println("hello  world");
}
public void print(){
System.out.println("print from class");
}
public void staticMethod(){
System.out.println("static from class");
}

public static void main(String[] args){
Second second=new Second();
second.hello();
second.print();
second.staticMethod();
FirstInterface.staticMethod();
}
}